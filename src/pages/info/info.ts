import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { PushappProvider } from "../../providers/pushapp/pushapp";

/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  currentLocation: string;
  pushapp: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, public pushappProvider: PushappProvider) {
    this.getPushapp();
    storage.get('currentLocation').then((data) => {
      if(data != null){
        this.currentLocation = data;
      }else{
        this.currentLocation = 'Wellington';
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoPage');
  }

  setLocation(index) {
    this.storage.set('currentLocation', 'Auckland');
    //this.currentLocationIndex = index;
    //let location = this.mapData[this.currentLocationIndex];
    //this.currentLocation = location.name;
    //var latLng = new google.maps.LatLng(location.lat, location.lng);
    //this.marker.setPosition(latLng);
    //this.map.panTo(latLng);
  }

  getPushapp(){
    this.storage.get('pushappId').then((value) => {
      let pushappId = value ? value : this.navParams.get("appId");
      if(pushappId){
        this.pushappProvider.get(pushappId).subscribe(
          (response: any) => {
            if(response.pushapp){
              console.log('----------->pushapp',response.pushapp)
              this.storage.set('pushappId', response.pushapp._id);
              this.pushapp = response.pushapp;
            }
          },
          err => {
            console.log(err);
            // let toast = this.toastCtrl.create({
            //   message: "Whoops, there was an error retrieving app",
            //   duration: 3000,
            //   position: "top"
            // });
            // toast.present();
          }
        );
      }
    }).catch(() => {});
  }

}
