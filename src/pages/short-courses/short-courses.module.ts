import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShortCoursesPage } from './short-courses';

@NgModule({
  declarations: [
    ShortCoursesPage,
  ],
  imports: [
    IonicPageModule.forChild(ShortCoursesPage),
  ],
})
export class ShortCoursesPageModule {}
