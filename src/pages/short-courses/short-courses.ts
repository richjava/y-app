import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { PushappProvider } from "../../providers/pushapp/pushapp";
import { SafeResourceUrl } from '@angular/platform-browser/src/security/dom_sanitization_service';

/**
 * Generated class for the ShortCoursesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-short-courses',
  templateUrl: 'short-courses.html',
})
export class ShortCoursesPage {
  pushapp: any;
  websiteUrl: SafeResourceUrl;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public sanitizer: DomSanitizer,
    private storage: Storage,
    public pushappProvider: PushappProvider
  ) {
    this.getPushapp();
    // set a key/value
  //storage.set('name', 'Max');
  // console.log('website page');
  // // Or to get a key/value pair
  // storage.get('pushappId').then((val) => {
  //   console.log('Your pushapp id is', val);
  // });
  }

  ionViewDidLoad() {}

  getPushapp(){
    this.storage.get('pushappId').then((value) => {
      let pushappId = value ? value : this.navParams.get("appId");
      if(pushappId){
        this.pushappProvider.get(pushappId).subscribe(
          (response: any) => {
            if(response.pushapp){
              this.storage.set('pushappId', response.pushapp._id);
              this.pushapp = response.pushapp;
              this.websiteUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.pushapp.websiteUrl);
              console.log(this.websiteUrl)
            }
          },
          err => {
            console.log(err);
            // let toast = this.toastCtrl.create({
            //   message: "Whoops, there was an error retrieving app",
            //   duration: 3000,
            //   position: "top"
            // });
            // toast.present();
          }
        );
      }
    }).catch(() => {});
  }

}
