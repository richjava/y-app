import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import { Storage } from '@ionic/storage';

import { PushappProvider } from "../../providers/pushapp/pushapp";

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  segment: "home/:appId", // used in browser as URL
  name: "HomePage"
})
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  isAnimate;
  pushapp: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pushappProvider: PushappProvider,
    public toastCtrl: ToastController,
    private storage: Storage
  ) {

    setTimeout(() => {
      this.isAnimate = true;
    }, 200);
  }

  ionViewDidLoad() {
    this.getPushapp();
    //console.log("ionViewDidLoad HomePage");
  }

  getPushapp(){
    this.storage.get('pushappId').then((value) => {
      let pushappId = value ? value : this.navParams.get("appId");
      if(pushappId){
        this.pushappProvider.get(pushappId).subscribe(
          (response: any) => {
            if(response.pushapp){
              this.storage.set('pushappId', response.pushapp._id);
              this.pushapp = response.pushapp;
            }
          },
          err => {
            console.log(err);
            // let toast = this.toastCtrl.create({
            //   message: "Whoops, there was an error retrieving app",
            //   duration: 3000,
            //   position: "top"
            // });
            // toast.present();
          }
        );
      }
    }).catch(() => {});
  }

  pushPage(page) {
    this.navCtrl.push(page);
  }
}
