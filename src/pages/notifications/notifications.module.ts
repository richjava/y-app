import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationsPage } from './notifications';
import { FromNowPipe } from '../../pipes/from-now/from-now';

@NgModule({
  declarations: [
    NotificationsPage,
    FromNowPipe
  ],
  imports: [
    IonicPageModule.forChild(NotificationsPage),
  ],
})
export class NotificationsPageModule {}
