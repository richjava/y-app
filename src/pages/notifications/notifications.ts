import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { PushappProvider } from "../../providers/pushapp/pushapp";
import { NotificationProvider } from "../../providers/notification/notification";

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
  notifications = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    public pushappProvider: PushappProvider,
    public notificationProvider: NotificationProvider
  ) {
    this.getNotifications();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationsPage');
  }

  getNotifications(){
    this.storage.get('pushappId').then((value) => {
      let pushappId = value ? value : this.navParams.get("appId");
      if(pushappId){
        console.log('getting pushes')
        this.notificationProvider.get(pushappId).subscribe(
          (response: any) => {
            console.log('notifications', response)
            if(response.notifications){

              this.notifications = response.notifications;

            }
          },
          err => {
            console.log(err);
          }
        );
      }
    }).catch(() => {});
  }

}
