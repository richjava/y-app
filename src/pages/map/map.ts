import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ToastController
} from "ionic-angular";
import { Storage } from "@ionic/storage";

import { PushappProvider } from "../../providers/pushapp/pushapp";
import { SegmentProvider } from "../../providers/segment/segment";
/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@IonicPage({
  segment: "map/:appId", // used in browser as URL
  name: "MapPage"
})
@Component({
  selector: "page-map",
  templateUrl: "map.html"
})
export class MapPage {
  @ViewChild("map") mapElement;
  map: any;
  lat: number;
  lng: number;
  btnColor = "primary";
  mapData: any;
  currentLocationIndex: number;
  marker: any;
  currentLocation: string;
  appName: string;

  segments = [];

  constructor(
    public nav: NavController,
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private pushappProvider: PushappProvider,
    public segmentProvider: SegmentProvider
  ) {
    this.getSegments();

    this.lat = -43.520407;
    this.lng = 172.56784;
    this.currentLocationIndex = 1;
    storage.get("currentLocation").then(data => {
      if (data != null) {
        this.currentLocation = data;
      } else {
        this.currentLocation = "Wellington";
      }
    });
    this.mapData = [
      {
        name: "Auckland",
        lat: -36.856838,
        lng: 174.764406
      },
      {
        name: "Wellington",
        lat: -41.29477,
        lng: 174.783618
      },
      {
        name: "Christchurch",
        lat: -43.520407,
        lng: 172.56784
      }
    ];
     let appId = this.navParams.get("appId") || '';
    //console.log("--------->appId:" + this.appId);
    //this.appId = "5a4874f182c2b8701b5b40d2";
    this.pushappProvider.get(appId).subscribe(
      (response: any) => {
        //this.navCtrl.push(MainPage);
        if(response.pushapp){
          this.appName = response.pushapp.name;
        }
       // console.log(response)
      },
      err => {
        console.log(err);
        //this.navCtrl.push(MainPage);
        // Unable to log in
        let toast = this.toastCtrl.create({
          message: "Whoops, there was an error",
          duration: 3000,
          position: "top"
        });
        toast.present();
      }
    );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MapPage");
    // this.initMap();
  }

  initMap(segment) {
    //let location = this.mapData[this.currentLocationIndex];
    let latLng = new google.maps.LatLng(segment.lat, segment.lng);
    this.currentLocation = segment.name;
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    this.marker = new google.maps.Marker({
      position: latLng,
      map: this.map
    });
  }
  // initMap() {
  //   let location = this.mapData[this.currentLocationIndex];
  //   let latLng = new google.maps.LatLng(location.lat, location.lng);
  //   this.currentLocation = location.name;
  //   let mapOptions = {
  //     center: latLng,
  //     zoom: 15,
  //     mapTypeId: google.maps.MapTypeId.ROADMAP
  //   };
  //   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

  //   this.marker = new google.maps.Marker({
  //     position: latLng,
  //     map: this.map
  //   });
  // }

  setLocation(segment) {
    // this.storage.set("currentLocation", segment.name);
    // this.currentLocationIndex = index;
    // let location = this.mapData[this.currentLocationIndex];
    // this.currentLocation = location.name;
    var latLng = new google.maps.LatLng(segment.lat, segment.lng);
    this.currentLocation = segment.name;
    this.marker.setPosition(latLng);
    this.map.panTo(latLng);
  }

  // setLocation(index) {
  //   this.storage.set("currentLocation", "Christchurch");
  //   this.currentLocationIndex = index;
  //   let location = this.mapData[this.currentLocationIndex];
  //   this.currentLocation = location.name;
  //   var latLng = new google.maps.LatLng(location.lat, location.lng);
  //   this.marker.setPosition(latLng);
  //   this.map.panTo(latLng);
  // }

  getSegments(){
    this.storage.get('pushappId').then((value) => {
      let pushappId = value ? value : this.navParams.get("appId");
      if(pushappId){
        this.segmentProvider.get(pushappId).subscribe(
          (response: any) => {
            console.log('segments', response)
            if(response.segments){
              //remove All Users segment
              // for(var i = 0; i < response.segments.length; i++){
              //   //console.log('loop', response.segments[i])
              //   if(response.segments[i].name === 'All users'){

              //     response.segments = response.segments.slice(i - 1, 1);
              //    // console.log('found', response.segments)
              //     break;
              //   }
              // }
              this.segments = response.segments;
              this.initMap(this.segments[0]);
            }
          },
          err => {
            console.log(err);
          }
        );
      }
    }).catch(() => {});
  }

  // ngAfterViewInit() {
  //   this.plt.ready().then(() => {
  //     this.initMap();
  //   });
  // }
}
