import { Injectable } from '@angular/core';

import { Item } from '../../models/item';
import { Api } from '../api/api';

@Injectable()
export class Items {

  constructor(public api: Api) { }

  query(params?: any) {
    return new Promise(resolve => {
      this.api.get('pushes', params).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
    //return this.http.get('http://swapi.co/api/films').map(res => res.json());
    //return this.api.get('pushes', params);
  }

  add(item: Item) {
  }

  delete(item: Item) {
  }

}
