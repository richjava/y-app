import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { Api } from '../api/api';

/*
  Generated class for the SegmentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SegmentProvider {

  constructor(public api: Api) { }


  query(appId) {

    const params = new HttpParams().set('pushapp', appId);
    let options = {
      "pushapp": appId
    };
    let seq = this.api.get('pushapps/'+ appId + '/segments').share();

    seq.subscribe((res: any) => {
      if (res.status == 'success') {
        console.log('success');
      } else {
        console.log('no success');
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  get(appId) {
    const params = new HttpParams().set('pushapp', appId);
    let options = {
      "pushapp":appId
    };
    let seq = this.api.get('pushapps/'+ appId + '/segments').share();

    seq.subscribe((res: any) => {
      // console.log(res);
      // if (res.notifications) {
      //   console.log('got notifications');
      // } else {
      //   console.log('not got notifications');
      // }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }


}
