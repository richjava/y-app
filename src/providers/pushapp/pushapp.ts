import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { Api } from '../api/api';

/*
  Generated class for the PushappProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PushappProvider {

  constructor(public api: Api) { }


  query() {
    let seq = this.api.get('pushapps').share();

    seq.subscribe((res: any) => {
      if (res.status == 'success') {
        console.log('success');
      } else {
        console.log('no success');
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  get(appId) {
    const params = new HttpParams().set('pushapp', appId);
    let options = {
      "pushapp":appId
    };
    let seq = this.api.get('pushapps/'+ appId).share();

    seq.subscribe((res: any) => {
      // console.log(res);
      // if (res.pushapp) {
      //   console.log('success');
      // } else {
      //   console.log('no success');
      // }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }


}
