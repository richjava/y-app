webpackJsonp([6],{

/***/ 320:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShortCoursesPageModule", function() { return ShortCoursesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__short_courses__ = __webpack_require__(463);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ShortCoursesPageModule = (function () {
    function ShortCoursesPageModule() {
    }
    return ShortCoursesPageModule;
}());
ShortCoursesPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__short_courses__["a" /* ShortCoursesPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__short_courses__["a" /* ShortCoursesPage */]),
        ],
    })
], ShortCoursesPageModule);

//# sourceMappingURL=short-courses.module.js.map

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShortCoursesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_pushapp_pushapp__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ShortCoursesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ShortCoursesPage = (function () {
    function ShortCoursesPage(navCtrl, navParams, sanitizer, storage, pushappProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.sanitizer = sanitizer;
        this.storage = storage;
        this.pushappProvider = pushappProvider;
        this.getPushapp();
        // set a key/value
        //storage.set('name', 'Max');
        // console.log('website page');
        // // Or to get a key/value pair
        // storage.get('pushappId').then((val) => {
        //   console.log('Your pushapp id is', val);
        // });
    }
    ShortCoursesPage.prototype.ionViewDidLoad = function () { };
    ShortCoursesPage.prototype.getPushapp = function () {
        var _this = this;
        this.storage.get('pushappId').then(function (value) {
            var pushappId = value ? value : _this.navParams.get("appId");
            if (pushappId) {
                _this.pushappProvider.get(pushappId).subscribe(function (response) {
                    if (response.pushapp) {
                        _this.storage.set('pushappId', response.pushapp._id);
                        _this.pushapp = response.pushapp;
                        _this.websiteUrl = _this.sanitizer.bypassSecurityTrustResourceUrl(_this.pushapp.websiteUrl);
                        console.log(_this.websiteUrl);
                    }
                }, function (err) {
                    console.log(err);
                    // let toast = this.toastCtrl.create({
                    //   message: "Whoops, there was an error retrieving app",
                    //   duration: 3000,
                    //   position: "top"
                    // });
                    // toast.present();
                });
            }
        }).catch(function () { });
    };
    return ShortCoursesPage;
}());
ShortCoursesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-short-courses',template:/*ion-inline-start:"D:\Projects\pushapps v3\app projects repo\y-app\src\pages\short-courses\short-courses.html"*/'<!--\n  Generated template for the ShortCoursesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Website</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content overflow-scroll="true">\n  <iframe *ngIf="websiteUrl" class="web-page" name= "eventsPage" [src]="websiteUrl" frameBorder="0">\n  </iframe>\n</ion-content>\n'/*ion-inline-end:"D:\Projects\pushapps v3\app projects repo\y-app\src\pages\short-courses\short-courses.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__providers_pushapp_pushapp__["a" /* PushappProvider */]])
], ShortCoursesPage);

//# sourceMappingURL=short-courses.js.map

/***/ })

});
//# sourceMappingURL=6.js.map