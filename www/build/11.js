webpackJsonp([11],{

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapPageModule", function() { return MapPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__map__ = __webpack_require__(454);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MapPageModule = (function () {
    function MapPageModule() {
    }
    return MapPageModule;
}());
MapPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */]),
        ],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_2__map__["a" /* MapPage */]
        ]
    })
], MapPageModule);

//# sourceMappingURL=map.module.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_pushapp_pushapp__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_segment_segment__ = __webpack_require__(215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MapPage = (function () {
    function MapPage(nav, toastCtrl, navCtrl, navParams, storage, pushappProvider, segmentProvider) {
        var _this = this;
        this.nav = nav;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.pushappProvider = pushappProvider;
        this.segmentProvider = segmentProvider;
        this.btnColor = "primary";
        this.segments = [];
        this.getSegments();
        this.lat = -43.520407;
        this.lng = 172.56784;
        this.currentLocationIndex = 1;
        storage.get("currentLocation").then(function (data) {
            if (data != null) {
                _this.currentLocation = data;
            }
            else {
                _this.currentLocation = "Wellington";
            }
        });
        this.mapData = [
            {
                name: "Auckland",
                lat: -36.856838,
                lng: 174.764406
            },
            {
                name: "Wellington",
                lat: -41.29477,
                lng: 174.783618
            },
            {
                name: "Christchurch",
                lat: -43.520407,
                lng: 172.56784
            }
        ];
        var appId = this.navParams.get("appId") || '';
        //console.log("--------->appId:" + this.appId);
        //this.appId = "5a4874f182c2b8701b5b40d2";
        this.pushappProvider.get(appId).subscribe(function (response) {
            //this.navCtrl.push(MainPage);
            if (response.pushapp) {
                _this.appName = response.pushapp.name;
            }
            // console.log(response)
        }, function (err) {
            console.log(err);
            //this.navCtrl.push(MainPage);
            // Unable to log in
            var toast = _this.toastCtrl.create({
                message: "Whoops, there was an error",
                duration: 3000,
                position: "top"
            });
            toast.present();
        });
    }
    MapPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad MapPage");
        // this.initMap();
    };
    MapPage.prototype.initMap = function (segment) {
        //let location = this.mapData[this.currentLocationIndex];
        var latLng = new google.maps.LatLng(segment.lat, segment.lng);
        this.currentLocation = segment.name;
        var mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.marker = new google.maps.Marker({
            position: latLng,
            map: this.map
        });
    };
    // initMap() {
    //   let location = this.mapData[this.currentLocationIndex];
    //   let latLng = new google.maps.LatLng(location.lat, location.lng);
    //   this.currentLocation = location.name;
    //   let mapOptions = {
    //     center: latLng,
    //     zoom: 15,
    //     mapTypeId: google.maps.MapTypeId.ROADMAP
    //   };
    //   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    //   this.marker = new google.maps.Marker({
    //     position: latLng,
    //     map: this.map
    //   });
    // }
    MapPage.prototype.setLocation = function (segment) {
        // this.storage.set("currentLocation", segment.name);
        // this.currentLocationIndex = index;
        // let location = this.mapData[this.currentLocationIndex];
        // this.currentLocation = location.name;
        var latLng = new google.maps.LatLng(segment.lat, segment.lng);
        this.currentLocation = segment.name;
        this.marker.setPosition(latLng);
        this.map.panTo(latLng);
    };
    // setLocation(index) {
    //   this.storage.set("currentLocation", "Christchurch");
    //   this.currentLocationIndex = index;
    //   let location = this.mapData[this.currentLocationIndex];
    //   this.currentLocation = location.name;
    //   var latLng = new google.maps.LatLng(location.lat, location.lng);
    //   this.marker.setPosition(latLng);
    //   this.map.panTo(latLng);
    // }
    MapPage.prototype.getSegments = function () {
        var _this = this;
        this.storage.get('pushappId').then(function (value) {
            var pushappId = value ? value : _this.navParams.get("appId");
            if (pushappId) {
                _this.segmentProvider.get(pushappId).subscribe(function (response) {
                    console.log('segments', response);
                    if (response.segments) {
                        //remove All Users segment
                        // for(var i = 0; i < response.segments.length; i++){
                        //   //console.log('loop', response.segments[i])
                        //   if(response.segments[i].name === 'All users'){
                        //     response.segments = response.segments.slice(i - 1, 1);
                        //    // console.log('found', response.segments)
                        //     break;
                        //   }
                        // }
                        _this.segments = response.segments;
                        _this.initMap(_this.segments[0]);
                    }
                }, function (err) {
                    console.log(err);
                });
            }
        }).catch(function () { });
    };
    return MapPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* ViewChild */])("map"),
    __metadata("design:type", Object)
], MapPage.prototype, "mapElement", void 0);
MapPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: "page-map",template:/*ion-inline-start:"D:\Projects\pushapps v3\app projects repo\y-app\src\pages\map\map.html"*/'<!--\n  Generated template for the MapPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Map</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div style="text-align: center;height: 42px;">\n      <ion-segment [(ngModel)]="currentLocation">\n          <ion-segment-button *ngFor="let segment of segments" [value]="segment.name" [innerHTML]="segment.name" (click)="setLocation(segment)">\n          </ion-segment-button>\n          <!-- <ion-segment-button value="Wellington" (click)="setLocation(1)">\n            Wellington\n          </ion-segment-button>\n          <ion-segment-button value="Christchurch" (click)="setLocation(2)">\n              Christchurch\n          </ion-segment-button> -->\n        </ion-segment>\n    </div>\n  <div #map style="height: calc(100% - 42px);"></div>\n</ion-content>\n'/*ion-inline-end:"D:\Projects\pushapps v3\app projects repo\y-app\src\pages\map\map.html"*/
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_3__providers_pushapp_pushapp__["a" /* PushappProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_pushapp_pushapp__["a" /* PushappProvider */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_4__providers_segment_segment__["a" /* SegmentProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_segment_segment__["a" /* SegmentProvider */]) === "function" && _g || Object])
], MapPage);

var _a, _b, _c, _d, _e, _f, _g;
//# sourceMappingURL=map.js.map

/***/ })

});
//# sourceMappingURL=11.js.map