webpackJsonp([15],{

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoPageModule", function() { return InfoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__info__ = __webpack_require__(449);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InfoPageModule = (function () {
    function InfoPageModule() {
    }
    return InfoPageModule;
}());
InfoPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__info__["a" /* InfoPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__info__["a" /* InfoPage */]),
        ],
    })
], InfoPageModule);

//# sourceMappingURL=info.module.js.map

/***/ }),

/***/ 449:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_pushapp_pushapp__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InfoPage = (function () {
    function InfoPage(navCtrl, navParams, storage, pushappProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.pushappProvider = pushappProvider;
        this.getPushapp();
        storage.get('currentLocation').then(function (data) {
            if (data != null) {
                _this.currentLocation = data;
            }
            else {
                _this.currentLocation = 'Wellington';
            }
        });
    }
    InfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InfoPage');
    };
    InfoPage.prototype.setLocation = function (index) {
        this.storage.set('currentLocation', 'Auckland');
        //this.currentLocationIndex = index;
        //let location = this.mapData[this.currentLocationIndex];
        //this.currentLocation = location.name;
        //var latLng = new google.maps.LatLng(location.lat, location.lng);
        //this.marker.setPosition(latLng);
        //this.map.panTo(latLng);
    };
    InfoPage.prototype.getPushapp = function () {
        var _this = this;
        this.storage.get('pushappId').then(function (value) {
            var pushappId = value ? value : _this.navParams.get("appId");
            if (pushappId) {
                _this.pushappProvider.get(pushappId).subscribe(function (response) {
                    if (response.pushapp) {
                        console.log('----------->pushapp', response.pushapp);
                        _this.storage.set('pushappId', response.pushapp._id);
                        _this.pushapp = response.pushapp;
                    }
                }, function (err) {
                    console.log(err);
                    // let toast = this.toastCtrl.create({
                    //   message: "Whoops, there was an error retrieving app",
                    //   duration: 3000,
                    //   position: "top"
                    // });
                    // toast.present();
                });
            }
        }).catch(function () { });
    };
    return InfoPage;
}());
InfoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-info',template:/*ion-inline-start:"D:\Projects\pushapps v3\app projects repo\y-app\src\pages\info\info.html"*/'<!--\n  Generated template for the InfoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Info</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div *ngIf="pushapp?.freePhone" style="text-align:center;padding: 30px 0;">\n    <button ion-button round icon-start>\n      <!-- <ion-icon class="home-btn--icon" name=\'ai-bell\'></ion-icon> -->\n      Call Freephone\n    </button>\n  </div>\n  <ion-card>\n\n    <!-- <ion-item>\n          <ion-avatar item-start>\n            <img src="https://randomuser.me/api/portraits/men/32.jpg">\n          </ion-avatar>\n          <h2>Marty McFly</h2>\n          <p>November 5, 1955</p>\n        </ion-item> -->\n\n    <img [src]="pushapp?.coverImageUrl">\n\n    <ion-card-content>\n      <p [innerHTML]="pushapp?.description"></p>\n    </ion-card-content>\n\n    <!-- <ion-row>\n          <ion-col>\n            <button ion-button icon-left clear small>\n              <ion-icon name="thumbs-up"></ion-icon>\n              <div>12 Likes</div>\n            </button>\n          </ion-col>\n          <ion-col>\n            <button ion-button icon-left clear small>\n              <ion-icon name="text"></ion-icon>\n              <div>4 Comments</div>\n            </button>\n          </ion-col>\n          <ion-col center text-center>\n            <ion-note>\n              11h ago\n            </ion-note>\n          </ion-col>\n        </ion-row> -->\n\n  </ion-card>\n  <div style="text-align: center;height: 42px;">\n    <ion-segment [(ngModel)]="currentLocation">\n      <ion-segment-button value="Auckland" (click)="setLocation(0)">\n        Auckland\n      </ion-segment-button>\n      <ion-segment-button value="Wellington" (click)="setLocation(1)">\n        Wellington\n      </ion-segment-button>\n      <ion-segment-button value="Christchurch" (click)="setLocation(2)">\n        Christchurch\n      </ion-segment-button>\n    </ion-segment>\n  </div>\n  <div padding>\n    <p>test</p>\n  </div>\n</ion-content>\n'/*ion-inline-end:"D:\Projects\pushapps v3\app projects repo\y-app\src\pages\info\info.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__providers_pushapp_pushapp__["a" /* PushappProvider */]])
], InfoPage);

//# sourceMappingURL=info.js.map

/***/ })

});
//# sourceMappingURL=15.js.map