webpackJsonp([16],{

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home__ = __webpack_require__(448);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = (function () {
    function HomePageModule() {
    }
    return HomePageModule;
}());
HomePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__home__["a" /* HomePage */]),
        ],
    })
], HomePageModule);

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 448:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_pushapp_pushapp__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HomePage = (function () {
    function HomePage(navCtrl, navParams, pushappProvider, toastCtrl, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pushappProvider = pushappProvider;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        setTimeout(function () {
            _this.isAnimate = true;
        }, 200);
    }
    HomePage.prototype.ionViewDidLoad = function () {
        this.getPushapp();
        //console.log("ionViewDidLoad HomePage");
    };
    HomePage.prototype.getPushapp = function () {
        var _this = this;
        this.storage.get('pushappId').then(function (value) {
            var pushappId = value ? value : _this.navParams.get("appId");
            if (pushappId) {
                _this.pushappProvider.get(pushappId).subscribe(function (response) {
                    if (response.pushapp) {
                        _this.storage.set('pushappId', response.pushapp._id);
                        _this.pushapp = response.pushapp;
                    }
                }, function (err) {
                    console.log(err);
                    // let toast = this.toastCtrl.create({
                    //   message: "Whoops, there was an error retrieving app",
                    //   duration: 3000,
                    //   position: "top"
                    // });
                    // toast.present();
                });
            }
        }).catch(function () { });
    };
    HomePage.prototype.pushPage = function (page) {
        this.navCtrl.push(page);
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: "page-home",template:/*ion-inline-start:"D:\Projects\pushapps v3\app projects repo\y-app\src\pages\home\home.html"*/'<!--\n  Generated template for the HomePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <div style="text-align: center;padding: 20px 0 50px 0; width: 100%">\n          <img [src]="pushapp?.logoImageUrl" />\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row style="position: relative; height: 60%">\n      <ion-col>\n        <div class="home-btns">\n          <button ion-button (click)="pushPage(\'ShortCoursesPage\')" class="home-btn top-left" [ngClass]="{\'in-left\': isAnimate}">\n        <ion-icon class="home-btn--icon" name=\'ai-earth\'></ion-icon>\n      </button>\n      <label class="home-btns--website" [ngClass]="{\'visible\': isAnimate}">Website</label>\n\n          <button ion-button (click)="pushPage(\'NotificationsPage\')" class="home-btn top-right" [ngClass]="{\'in-right\': isAnimate}">\n        <ion-icon class="home-btn--icon" name=\'ai-bell\'></ion-icon>\n    </button>\n    <label class="home-btns--notifications" [ngClass]="{\'visible\': isAnimate}">Notifications</label>\n\n          <button ion-button (click)="pushPage(\'MapPage\')" class="home-btn bottom-left" [ngClass]="{\'in-left\': isAnimate}">\n        <ion-icon class="home-btn--icon" name=\'ai-location\'></ion-icon>\n    </button>\n    <label class="home-btns--map" [ngClass]="{\'visible\': isAnimate}">Map</label>\n          <button ion-button  (click)="pushPage(\'InfoPage\')" class="home-btn bottom-right" [ngClass]="{\'in-right\': isAnimate}">\n        <ion-icon class="home-btn--icon" name=\'ai-info\'></ion-icon>\n    </button>\n    <label class="home-btns--info" [ngClass]="{\'visible\': isAnimate}">Info</label>\n        </div>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <!-- <ion-row>\n      <ion-col col-6>\n        <button ion-button color="primary" clear small icon-start>\n          <ion-icon class="home-btn" name=\'thumbs-up\'></ion-icon>\n        </button>\n      </ion-col>\n      <ion-col col-6>\n        <button ion-button color="primary" clear small icon-start>\n          <ion-icon class="home-btn" name=\'thumbs-up\'></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-6>\n        <button ion-button color="primary" clear small icon-start>\n          <ion-icon class="home-btn" name=\'thumbs-up\'></ion-icon>\n        </button>\n      </ion-col>\n      <ion-col col-6>\n        <button ion-button color="primary" clear small icon-start>\n          <ion-icon class="home-btn" name=\'thumbs-up\'></ion-icon>\n        </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid> -->\n</ion-content>\n'/*ion-inline-end:"D:\Projects\pushapps v3\app projects repo\y-app\src\pages\home\home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_pushapp_pushapp__["a" /* PushappProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=16.js.map